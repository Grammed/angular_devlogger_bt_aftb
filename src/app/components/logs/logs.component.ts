import { Component, OnInit } from '@angular/core';
import { Log } from '../../models/Log';
import {LogService} from '../../services/log.service';


@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent implements OnInit {
  logs: Log[];
  selectedLog: Log;
  loaded = false;

  constructor(private logService: LogService) { }


  ngOnInit(): void {
    this.logService.stateClear.subscribe((currentStateClear => {
      if (currentStateClear) {
        this.selectedLog = {id: '', text: '', date: ''};
      }
    }));

    this.logService.getLogs().subscribe(logs => {
      this.logs = logs;
      // #3
      this.loaded = true;
    });
  }

  onSelect(log: Log): void {
    this.logService.setFormLog(log);
    this.selectedLog = log;
  }

  onDelete(log: Log): void {
    if (confirm('Are you sure?')) {
    this.logService.deleteLog(log);
    }
  }
}
