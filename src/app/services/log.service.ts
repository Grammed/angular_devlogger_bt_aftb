import { Injectable } from '@angular/core';
import {Log} from '../models/Log';
import {BehaviorSubject, Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LogService {
  logs: Log[];
  private logSource = new BehaviorSubject({id: null, text: null, date: null});
  selectedLog = this.logSource.asObservable();
  private stateSource = new BehaviorSubject<boolean>(true);
  stateClear = this.stateSource.asObservable();

  constructor() {
    /*this.logs =
      [
        {id: '001', text: 'Generated components',  date: new Date('2017-12-17T03:24:00')},
        {id: '002', text: 'Added Bootstrap',  date: new Date('2018-06-20T19:59:59')},
        {id: '003', text: 'Added Logs Component',  date: new Date('2019-05-03T23:33:01')}
      ];*/
    this.logs = [];
  }
  getLogs(): Observable<Log[]> {
    // #2 Get logs and parse back to JSON.
    if (localStorage.getItem('logs') === null) {
      this.logs = [];
    } else {
      this.logs = JSON.parse(localStorage.getItem('logs'));
    }
    // #3 sort return by date.
    return of (this.logs.sort((a, b) => {
      return b.date - a.date;
    }));
  }
  setFormLog(log: Log): void {
    this.logSource.next(log);
  }
  addLog(log: Log): void {
    this.logs.unshift(log);
    // #1Add to local storage:
    localStorage.setItem('logs', JSON.stringify(this.logs));
  }
  updateLog(log: Log): void {
   this.logs.forEach((current, index) => {
     if (current.id === log.id) {
       this.logs.splice(index, 1);
     }
   });
   this.logs.unshift(log);
   localStorage.setItem('logs', JSON.stringify(this.logs));
  }
  deleteLog(log: Log): void {
    this.logs.forEach((current, index) => {
      if (current.id === log.id) {
        this.logs.splice(index, 1);
      }
    });
    localStorage.setItem('logs', JSON.stringify(this.logs));
  }
  clearState(): void {
    this.stateSource.next(true);
  }
}
